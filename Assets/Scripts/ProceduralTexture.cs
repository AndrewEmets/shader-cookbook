﻿using UnityEngine;
using System.Collections;

public class ProceduralTexture : MonoBehaviour
{
    public int TextureSize = 512;
    public int a = 1;
    private Texture2D generatedTexture;
    
    private Vector2 centerPosition;

    private Renderer renderer;

    private void Awake()
    {
    }

    [ContextMenu("Start")]
    private void Start()
    {
        generatedTexture = GenerateGradient();
        renderer = GetComponent<Renderer>();

        renderer.sharedMaterial.SetTexture("_MainTex", generatedTexture);
    }

    public void OnValidate()
    {
        Start();
    }

    private Texture2D GenerateGradient()
    {
        if (generatedTexture != null)
        {
            DestroyImmediate(generatedTexture);
        }

        centerPosition = Vector2.one * 0.5f * TextureSize;
        var texture = new Texture2D(TextureSize, TextureSize);
        texture.filterMode = FilterMode.Trilinear;

        for (int i = 0; i < TextureSize; i++)
        {
            for (int j = 0; j < TextureSize; j++)
            {
                Vector2 currentPosition = new Vector2(j, i);
                var distance = Vector2.Distance(centerPosition, currentPosition) / (TextureSize / 2f);
                distance = Mathf.Abs(1 - Mathf.Clamp01(distance));
                distance = Mathf.Sin(distance * a);
                var color = new Color(distance, distance, distance, 1.0f);
                texture.SetPixel(j, i, color);
            }
        }
        texture.name = "Gradient";
        texture.Apply();
        return texture;
    }

    public void OnDestroy()
    {
        Destroy(generatedTexture);
    }
}
