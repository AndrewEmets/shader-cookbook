﻿Shader "Custom/08 - Specular Blinn Phong" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SpecColor("Specular color", Color) = (1,1,1,1)
		_SpecPower("Specular power", Range(0,10)) = 0.5
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		
		//#pragma surface surf Standard fullforwardshadows
		#pragma surface surf CustomBlinnPhong

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		};

		
		fixed4 _Color;
		fixed _SpecPower;

		inline fixed4 LightingCustomBlinnPhong(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			float3 halfVector = normalize(lightDir + viewDir);

			float diff = max(0, dot(s.Normal, lightDir));

			float nh = max(0, dot(s.Normal, halfVector));
			float spec = pow(nh, _SpecPower) * _SpecColor;

			float4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * diff) + (_LightColor0.rgb * _SpecColor.rgb * spec) * atten;
			
			c.a = s.Alpha;

			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{			
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Specular = _SpecPower;
			o.Gloss = 1;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG		
	}

	FallBack "Diffuse"
}
