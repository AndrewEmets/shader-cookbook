﻿Shader "Custom/07 - Levels" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_inGamma("Input gamma", Range(0, 2)) = 1.61
		_inBlack("Input black", Range(0, 255)) = 0
		_inWhite("Input white", Range(0, 255)) = 255
		_outBlack("Output black", Range(0, 255)) = 0
		_outWhite("Output white", Range(0, 255)) = 255
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		};

		fixed4 _Color;
		float _inGamma;
		float _inBlack;
		float _inWhite;
		float _outBlack;
		float _outWhite;
		
		float GetPixelLevel(float pixelColor)
		{
			float pixelResult = pixelColor * 255.0;
			pixelResult = max(0, pixelResult - _inBlack);
			pixelResult = saturate(pow(pixelResult / (_inWhite - _inBlack), _inGamma));
			pixelResult = (pixelResult * (_outWhite - _outBlack) + _outBlack) / 255.0;

			return pixelResult;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color+_Color;
			
			float r = GetPixelLevel(c.r);
			float g = GetPixelLevel(c.g);
			float b = GetPixelLevel(c.b);

			o.Albedo = float3(r,g,b);
			o.Alpha = c.a;
		}

		ENDCG
	}
	FallBack "Diffuse"
}
