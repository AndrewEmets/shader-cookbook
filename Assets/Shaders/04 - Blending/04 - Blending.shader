﻿Shader "Custom/04 - Blending" 
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_ColorA("Color A", Color) = (1,1,1,1)
		_ColorB("Color B", Color) = (1,1,1,1)
		_RTex("R Texture", 2D) = ""
		_GTex("G Texture", 2D) = ""
		_BTex("B Texture", 2D) = ""
		_BlendTex("Blend Texture", 2D) = ""
	}

	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard 

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		struct Input 
		{
			float2 uv_RTex;
			float2 uv_GTex;
			float2 uv_BTex;
			float2 uv_BlendTex;
		};

		fixed4 _Color;
		fixed4 _ColorA;
		fixed4 _ColorB;

		uniform sampler2D _RTex;
		uniform sampler2D _GTex;
		uniform sampler2D _BTex;
		uniform sampler2D _BlendTex;

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			float4 blendData = tex2D(_BlendTex, IN.uv_BlendTex);

			float4 rTexData = tex2D(_RTex, IN.uv_RTex);
			float4 gTexData = tex2D(_GTex, IN.uv_GTex);
			float4 bTexData = tex2D(_BTex, IN.uv_BTex);

			float4 finalColor;
			finalColor = lerp(rTexData, gTexData, blendData.g);
			finalColor = lerp(finalColor, bTexData, blendData.b);
			finalColor.a = 1.0;

			float4 terrainLayers = lerp(_ColorA, _ColorB, blendData.r);
			finalColor *= terrainLayers;
			finalColor = saturate(finalColor);

			o.Albedo = finalColor.rgb * _Color.rgb;
			o.Alpha = finalColor.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
