﻿Shader "Custom/05 - Normals" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = ""{}
		_NormalTex("Normal map", 2D) = ""{}
		_NormalIntencity ("Normal map intencity", Range(0, 2)) = 1
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		
		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_NormalTex;
		};

		fixed4 _Color;
		sampler2D _MainTex;
		sampler2D _NormalTex;
		float _NormalIntencity;

		void surf(Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			
			float3 normalVector = UnpackNormal(tex2D(_NormalTex, IN.uv_NormalTex));
			normalVector = float3(normalVector.x * _NormalIntencity, normalVector.y * _NormalIntencity, normalVector.z);

			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Normal = normalVector.xyz;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
