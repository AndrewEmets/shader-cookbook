﻿Shader "Custom/03 - Sprite sheet animation" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_CellAmount("Cell amount", float) = 0.0
		_Speed ("Speed", Range(0.01, 32)) = 12
	}
	SubShader 
	{
		Tags 
		{
			"RenderType"="Transparent" 
			"Queue"="Transparent"
		}
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		
		uniform fixed4 _Color;
		uniform float _CellAmount;
		uniform float _Speed;

		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			float2 spriteUV = IN.uv_MainTex;
			float cellUVPercentage = 1 / _CellAmount;
			float frame = fmod(_Time.y * _Speed, _CellAmount);
			frame = floor(frame);
			float xValue = (spriteUV.x + frame) * cellUVPercentage;
			spriteUV = float2(xValue, spriteUV.y);
			
			fixed4 c = tex2D(_MainTex, spriteUV) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
