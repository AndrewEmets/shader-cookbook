﻿Shader "Cookbook/02 - UV_Tutorial" {
	Properties 
	{
		_TintColor("Tint Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		_ScrollSpeedX("X Scroll Speed", Range(-10, 10)) = 2
		_ScrollSpeedY("Y Scroll Speed", Range(-10, 10)) = 2

		

	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		};

		fixed4 _TintColor;
		float _ScrollSpeedX;
		float _ScrollSpeedY;
		
		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			fixed2 scrollUV = IN.uv_MainTex;
			fixed xScrollValue = _ScrollSpeedX * _Time.x;
			fixed yScrollValue = _ScrollSpeedY * _Time.x;
			scrollUV += fixed2(xScrollValue, yScrollValue);
			
			fixed4 c = tex2D (_MainTex, scrollUV) * _TintColor;
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
