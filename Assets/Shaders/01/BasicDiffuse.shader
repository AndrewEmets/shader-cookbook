﻿Shader "Cookbook/01 - BasicDiffuse" 
{
	Properties
	{
		_RampTex("Ramp Diffuse", 2D) = "white" {}
	}

	SubShader
	{
		Cull back
		Blend SrcAlpha OneMinusSrcAlpha
		Tags { "RenderType" = "Transparent" }
		CGPROGRAM
		
		#pragma surface surf BasicDiffuse
		
		uniform sampler2D _RampTex;

		half4 LightingBasicDiffuse(SurfaceOutput s, half3 lightDir, half3 viewDir, fixed atten)
		{
			float diffLight = max(0, dot(s.Normal, lightDir));
			float rimLight = max(0, dot(s.Normal, viewDir));
			float3 ramp = tex2D(_RampTex, float2(rimLight, diffLight*0.5)).rgb;
			float4 c;

			c.rgb = s.Albedo * _LightColor0.rgb *  ramp;
			c.a = s.Alpha;
			
			return c;
		}

		struct Input 
		{
			float2 uv_MainTex;
		};


		void surf(Input IN, inout SurfaceOutput o) 
		{
			o.Albedo = float4(1, 1, 1, 1);
		}

		ENDCG
	}

	Fallback "Diffuse"
}
