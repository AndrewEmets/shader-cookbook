﻿Shader "Custom/09 - Specular Phong" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SpecColor("Specular color", Color) = (1,1,1,1)
		_SpecPower("Specular power", Range(0,10)) = 0.5
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		
		//#pragma surface surf Standard fullforwardshadows
		#pragma surface surf Phong

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		};

		
		fixed4 _Color;
		fixed _SpecPower;
		
		void surf (Input IN, inout SurfaceOutput o) 
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Specular = _SpecPower;
			o.Gloss = 1;
			o.Albedo = c.rgb;			
			o.Alpha = c.a;
		}

		inline fixed4 LightingPhong(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			float diff = dot(s.Normal, lightDir);
			float3 reflectionVector = normalize(2 * s.Normal * diff - lightDir);

			float spec = pow(max(0, dot(reflectionVector, viewDir)), _SpecPower);
			float3 finalSpec = _SpecColor.rgb * spec;


			fixed4 c;
			float3 x = _LightColor0.rgb;
			c.rgb = (s.Albedo * _LightColor0.rgb * diff) + (_LightColor0.rgb * finalSpec) * atten;
			
			c.a = 1;
			return c;
		}
		ENDCG		
	}

	FallBack "Diffuse"
}
